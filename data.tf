#### Data Lookup for Linux #####
data "aws_ami" "linux_ami" {
  most_recent = true
  #owners      = [data.aws_caller_identity.current.account_id]
  owners      = ["793022030544"]   # Use Shared AMI
  name_regex  = "RHEL8_.?-CFG-AMI-*"
  tags = {
    Name = "RHEL 8.5 N"
  }
  filter {
    name   = "block-device-mapping.encrypted"
    values = ["true"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

#### Data Lookup for Windows #####
data "aws_ami" "windows_ami" {
  most_recent = true
  owners      = ["793022030544"]   # Use Shared AMI
  name_regex  = "WIN19-CFG-AMI-FULL-*"
  tags = {
    Name = "WIN19-CFG-AMI-FULL-N"
  }

  filter {
    name   = "block-device-mapping.encrypted"
    values = ["true"]

  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}


data "aws_subnets" "private" {
  filter {
    name = "vpc-id"
    values = [var.vpc_id]
  }

  filter {
    name   = "tag:Name"
    values = ["*-jump-*"] # example for app subnet
  }
}

data "aws_security_groups" "linux_sg_internal" {

  filter {
    name   = "group-name"
    values = ["linux-sg"] # example for linux-sg
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }
}

data "aws_security_groups" "windows_sg_internal" {

  filter {
    name   = "group-name"
    values = ["windows-sg"] # example for windows-sg
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }
}

data "aws_kms_alias" "ebs_kms_id" {
    name = replace(var.kms_key, "arn:aws:kms:${var.region}:${data.aws_caller_identity.current.account_id}:", "")
}

data "aws_security_groups" "sg_FSx" { 
  filter {
    name   = "group-name"
    values = ["*-sg-FSx"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  filter {
    name   = "tag:ApplicationID"
    values = ["${lookup(var.mandatory_tags, "ApplicationID")}"]
  }
}