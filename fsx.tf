module "fsx_label" {
  source  = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/label-module/cfg"
  version = "2.1.20"
  os               = var.os
  name              = var.fsx_name_ana
  mandatory_tags    = var.mandatory_tags
  environment       = var.app_environment
}

## FSx Module reference 
module "fsx" {
  source  = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/fsx-module/aws" 
  version = "0.2.15" 
  os               = var.os
  ## FSx details
  kms_key_id                             = var.kms_key_id
  storage_capacity                       = var.storage_capacity
  subnet_ids                             = var.fsx_subnet_ids
  throughput_capacity                    = var.throughput_capacity
  security_group_ids                     = concat(data.aws_security_groups.sg_FSx.ids, var.security_group_ids)
  automatic_backup_retention_days        = var.automatic_backup_retention_days
  deployment_type                        = var.deployment_type
  preferred_subnet_id                    = var.preferred_subnet_id
  storage_type                           = var.storage_type
  dns_ips                                = var.dns_ips
  domain_name                            = var.domain_name
  file_system_administrators_group       = var.file_system_administrators_group
  organizational_unit_distinguished_name = var.organizational_unit_distinguished_name
  password                               = var.password
  username                               = var.username
  skip_final_backup                      = var.skip_final_backup
  tags                                   = module.fsx_label.tags
}