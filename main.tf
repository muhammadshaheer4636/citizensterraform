module "label" {
  source  = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/label-module/cfg"
  version = "2.1.28"

  name            = var.app_name
  environment     = var.app_environment
  mandatory_tags  = var.mandatory_tags
  attributes      = var.attributes
  additional_tags = var.additional_tags
  region          = var.region
}


# Conditional EC2 instance for Linux
   module "ec2-instance-linux" {
     source           = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/ec2-module/aws"
     version          = "2.3.22"
     count            = var.os == "linux" ? 1 : 0
     # Linux EC2 Details
     os               = var.os
     instance_count   = var.instance_count
     ami_id           = data.aws_ami.linux_ami.id
     instance_type    = var.instance_type
     key_name         = var.key_name
     vpc_security_group_ids = concat(data.aws_security_groups.linux_sg_internal.ids, var.vpc_security_group_ids)
     iam_instance_profile   = var.iam_instance_profile
     subnet_ids       = data.aws_subnets.private.ids
     backup_plan      = var.backup_plan
     tags             = merge(module.label.tags, {"Name" = "${var.app_name}-${lookup(var.mandatory_tags, "ApplicationID")}-${var.app_environment}-${var.region}-instance-app"})
     use_num_suffix   = var.use_num_suffix
   }

   # Conditional EC2 instance for Windows
   module "ec2-instance-windows" {
     source           = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/ec2-module/aws"
     version          = "2.3.22"
     count            = var.os == "windows" ? 1 : 0
     # Windows EC2 Details
     os               = var.os
     instance_count   = var.instance_count
     ami_id           = data.aws_ami.windows_ami.id
     instance_type    = var.instance_type
     key_name         = var.key_name
     vpc_security_group_ids = concat(data.aws_security_groups.windows_sg_internal.ids, var.vpc_security_group_ids)
     iam_instance_profile   = var.iam_instance_profile
     subnet_ids       = data.aws_subnets.private.ids
     backup_plan      = var.backup_plan
     tags             = merge(module.label.tags, {"Name" = "${var.app_name}-${lookup(var.mandatory_tags, "ApplicationID")}-${var.app_environment}-${var.region}-instance-app"})
     use_num_suffix   = var.use_num_suffix
   }


# Tessell Control Plane endpoint Creation
   ##########################################
   module "vpcendpoint_cp_label" {
     source = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/label-module/cfg"
     version = "2.1.20"
     name = var.vpc_endpoint_cp_full_name
     environment = var.vpc_endpoint_cp_environment
     mandatory_tags = var.vpc_endpoint_cp_mandatory_tags
     attributes = var.vpc_endpoint_cp_attributes
     additional_tags = var.vpc_endpoint_cp_additional_tags
     snowapi_disable = var.vpc_endpoint_cp_snowapi_disable
   }

   module "vpc_endpoint_cp" {
     source = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/vpc-endpoint-module/aws"
     version = "1.0.11"
     vpc_endpoint_vpc_id = var.vpc_endpoint_cp_vpc_id
     vpc_endpoint_service_name = var.vpc_endpoint_cp_service_name
     vpc_endpoint_type = var.vpc_endpoint_cp_type
     vpc_endpoint_security_group_ids = var.vpc_endpoint_cp_security_group_ids
     vpc_endpoint_subnet_ids = var.vpc_endpoint_cp_subnet_ids
     vpc_endpoint_private_dns_enabled = var.vpc_endpoint_cp_private_dns_enabled
     vpc_endpoint_full_name = var.vpc_endpoint_cp_full_name
     tags = module.vpcendpoint_cp_label.tags
     vpc_endpoint_route_table_ids = var.vpc_endpoint_cp_route_table_ids
   }

   ##########################################
   # Tessell API endpoint Creation
   ##########################################
   module "vpcendpoint_api_label" {
     source = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/label-module/cfg"
     version = "2.1.20"
     name = var.vpc_endpoint_api_full_name
     environment = var.vpc_endpoint_api_environment
     mandatory_tags = var.vpc_endpoint_api_mandatory_tags
     attributes = var.vpc_endpoint_api_attributes
     additional_tags = var.vpc_endpoint_api_additional_tags
     snowapi_disable = var.vpc_endpoint_api_snowapi_disable
   }

   module "vpc_endpoint_drprim" {
     source = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/vpc-endpoint-module/aws"
     version = "1.0.11"
     vpc_endpoint_vpc_id = var.vpc_endpoint_api_vpc_id
     vpc_endpoint_service_name = var.vpc_endpoint_api_service_name
     vpc_endpoint_type = var.vpc_endpoint_api_type
     vpc_endpoint_security_group_ids = var.vpc_endpoint_api_security_group_ids
     vpc_endpoint_subnet_ids = var.vpc_endpoint_api_subnet_ids
     vpc_endpoint_private_dns_enabled = var.vpc_endpoint_api_private_dns_enabled
     vpc_endpoint_full_name = var.vpc_endpoint_api_full_name
     tags = module.vpcendpoint_api_label.tags
     vpc_endpoint_route_table_ids = var.vpc_endpoint_api_route_table_ids
   }