# ec2 instance
output "id" {
  value = module.ec2-instance.id
}

output "private_ip" {
  value = module.ec2-instance.private_ip
}