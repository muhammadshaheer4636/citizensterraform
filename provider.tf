data "terraform_remote_state" "vault_creds" {
  backend = "remote"
  config = {
    organization = "cfg-cloud-services"
    hostname     = "terraform-cldsvc-prod.corp.internal.citizensbank.com"
    workspaces = {
      name = var.vault_creds_workspace #Name of the TFE workspace which ISN't the name of the Vault namespace
    }
  }
}

provider "vault" {
  address = var.vault_address
  auth_login {
    path      = "auth/approle/login"
    namespace = data.terraform_remote_state.vault_creds.outputs.service_namespace
    parameters = {
      role_id   = data.terraform_remote_state.vault_creds.outputs.approle_roles["terraform"].role_id
      secret_id = data.terraform_remote_state.vault_creds.outputs.approle_roles["terraform"].secret_id
    }
  }
}

data "vault_aws_access_credentials" "creds" {
  backend = data.terraform_remote_state.vault_creds.outputs.aws_secrets_path
  role    = var.vault_role_name
  type    = "sts"
  ttl     = "60m"
}

provider "aws" {
  region     = var.region
  access_key = data.vault_aws_access_credentials.creds.access_key
  secret_key = data.vault_aws_access_credentials.creds.secret_key
  token      = data.vault_aws_access_credentials.creds.security_token
}

data "aws_iam_account_alias"  "current" {}
data "aws_caller_identity"    "current" {}