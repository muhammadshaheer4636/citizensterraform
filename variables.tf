### LABEL MODULE VARS ###

variable "app_name" {
  type        = string
  description = "Resource name according to the Cloud Naming Conventions"
}

variable "os" {
     description = "The operating system, either 'windows' or 'linux'."
     type        = string
     default     = "linux"
   }
   
variable "app_environment" {
  type        = string
  description = "Stage, e.g. 'DEV', 'SIT', 'QA', 'PROD', 'DR'"
}

variable "mandatory_tags" {
  type        = map(string)
  description = "Map of mandatory tags according to the Cloud Tagging Standard, excepting Name, Environment and BunsinessUnit"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `department`, `stage`, `name` and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "List of additional attributes (e.g. [`global`])"
}

variable "additional_tags" {
  type        = map(string)
  default     = {}
  description = "Map of additional tags (e.g. `map('foo','bar')`"
}

variable "snowapi_disable" {
  type        = bool
  default     = false
}

### EC2 MODULE VARS ###

variable "os" {
  type = string
  default = ""
}

variable "instance_count" {
  type        = number
  description = "Number of instances to launch"
  default     = 1
}

variable "ami_id" {
  type        = string
  description = "AMI id"
  default     = ""
}

variable "instance_type" {
  type        = string
  description = "ec2 instance type"
  default     = "t2.micro"
}

variable "key_name"{
  type        = string
  description = "key pair name"
}

variable "iam_instance_profile" {
  type        = string
  description = "The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile."
}

variable "backup_plan" {
  type        = string
  description = "BackupPlan tag for the volumes attached to the instances: Prod/NonProd/Exempt"
}

variable "subnet_id" {
  type        = string
  description = "The VPC Subnet ID to launch in"
  default     = ""
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "List of security groups for the ec2 instance"
}

variable "subnet_ids" {
  type        = list(string)
  description = "A list of VPC Subnet IDs to launch in"
  default     = []
}

variable "use_num_suffix" {
  description = "Always append numerical suffix to instance name, even if instance_count is 1"
  type        = bool
  default     = true
}

variable "num_suffix_format" {
  description = "Numerical suffix format used as the volume and EC2 instance name suffix"
  type        = string
  default     = "-%d"
}

variable "kms_key" {
  description = "KMS encrypt key"
  type = string
  default = ""
}

variable "vpc_id" {
  description = "VPC the ec2 instance to be launched in"
  type        = string
  default     = ""
}

# Region
variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"  
}

### Vault Role ###
variable "vault_role_name" {
  description = "Enter Vault Role name"
  type        = string
}

variable "vault_address" {
  description = "vault address"
  type        = string
  default     = ""
}

variable "vault_creds_workspace" {
  description = "vault creds workspace"
  type        = string
  default     = ""
}

########################################## Tessell Control Plane endpoint Creation ##########################################

variable "vpc_endpoint_cp_environment" {
  description = "The key name to use for the instance"
  type        = string
}

variable "vpc_endpoint_cp_attributes" {
  description = "Additional attributes"
  type        = list(string)
  default     = []
}

variable "vpc_endpoint_cp_mandatory_tags" {
  description = "Map with the mandatory tags"
  type        = map(string)
}

variable "vpc_endpoint_cp_additional_tags" {
  description = "Map of Key-Value-Pairs describing any additional Tags to place on Resources"
  type        = map(string)
  default     = {}
}

variable "vpc_endpoint_cp_snowapi_disable" {
  type        = bool
  default     = false
}


# variable "tags" {
#   description = "A mapping of tags to assign to the resource"
#   type        = map(string)
#   default     = {}
# }

######## VPC Endpoint

variable "vpc_endpoint_cp_vpc_id" {
  description = "VPC ID to deploy resources into"
  type        = string
}

variable "vpc_endpoint_cp_service_name" {
  description = "vpc endpoints service name"
  type        = string
}

variable "vpc_endpoint_cp_type" {
  description = "vpc endpoint type"
  type        = string
}

variable "vpc_endpoint_cp_security_group_ids" {
  type        = list(string)
  description = "Which SGs to add to"
}

variable "vpc_endpoint_cp_subnet_ids" {
  type        = list(string)
  description = "Subnets to be included into the endpoint"
}

variable "vpc_endpoint_cp_private_dns_enabled" {
  type        = bool
  description = "private dns enabled?"
  default     = "false"
}

variable "vpc_endpoint_cp_full_name" {
  type        = string
  description = "VPC endpoint full name"
}

### End of Private link variables


### Gateway Endpoint Variables ###

variable "vpc_endpoint_cp_route_table_ids" {
	type = list(string)
    default = null
    description = "Gateway Endpoint Route Table"
}


########################################## Tessell API endpoint Creation ##########################################

variable "vpc_endpoint_api_environment" {
  description = "The key name to use for the instance"
  type        = string
}

variable "vpc_endpoint_api_attributes" {
  description = "Additional attributes"
  type        = list(string)
  default     = []
}

variable "vpc_endpoint_api_mandatory_tags" {
  description = "Map with the mandatory tags"
  type        = map(string)
}

variable "vpc_endpoint_api_additional_tags" {
  description = "Map of Key-Value-Pairs describing any additional Tags to place on Resources"
  type        = map(string)
  default     = {}
}

variable "vpc_endpoint_api_snowapi_disable" {
  type        = bool
  default     = false
}


# variable "tags" {
#   description = "A mapping of tags to assign to the resource"
#   type        = map(string)
#   default     = {}
# }

######## VPC Endpoint

variable "vpc_endpoint_api_vpc_id" {
  description = "VPC ID to deploy resources into"
  type        = string
}

variable "vpc_endpoint_api_service_name" {
  description = "vpc endpoints service name"
  type        = string
}

variable "vpc_endpoint_api_type" {
  description = "vpc endpoint type"
  type        = string
}

variable "vpc_endpoint_api_security_group_ids" {
  type        = list(string)
  description = "Which SGs to add to"
}

variable "vpc_endpoint_api_subnet_ids" {
  type        = list(string)
  description = "Subnets to be included into the endpoint"
}

variable "vpc_endpoint_api_private_dns_enabled" {
  type        = bool
  description = "private dns enabled?"
  default     = "false"
}

variable "vpc_endpoint_api_full_name" {
  type        = string
  description = "VPC endpoint full name"
}

### End of Private link variables


### Gateway Endpoint Variables ###

variable "vpc_endpoint_api_route_table_ids" {
	type = list(string)
    default = null
    description = "Gateway Endpoint Route Table"
}